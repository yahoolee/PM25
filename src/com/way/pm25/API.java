package com.way.pm25;

import java.util.HashMap;

public class API {
	public static final String BAIDU_GEOCODER_API = "http://api.map.baidu.com/geocoder/v2/?ak=1919b4b56ae680106f8d1dfa5f7244d9&location=%s5&output=json";
	// http://api.map.baidu.com/geocoder/v2/?ak=1919b4b56ae680106f8d1dfa5f7244d9&location=22.5329294,114.019329&output=json
	public static final String GOOGLE_GEOCODER_API = "http://maps.googleapis.com/maps/api/geocode/json?latlng=%s&sensor=true";
	// http://maps.googleapis.com/maps/api/geocode/json?latlng=22.5329294,114.019329&sensor=true
	public static final String PM25_API = "http://www.pm25.in/api/querys/aqi_details.json?token=4esfG6UEhGzNkbszfjAp&city=%s&stations=yes";
	// http://www.pm25.in/api/querys/aqi_details.json?token=4esfG6UEhGzNkbszfjAp&city=shenzhen&stations=yes
	public static final String PM25_FEEDBACK_API = "http://www.pm25.in/api/update/feedback.json?token=4esfG6UEhGzNkbszfjAp&city";
	public static final String[] CITIES = { "change city", "auto", "baoding",
			"baoji", "baotou", "beihai", "beijing", "binzhou", "cangzhou",
			"changchun", "changsha", "changzhi", "changzhou", "chengde",
			"chengdou", "chongqing", "dalian", "dandong", "datong", "dezhou",
			"dongguan", "dongying", "eerduosi", "foshan", "fuzhou",
			"guangzhou", "guiyang", "haerbin", "haikou", "handan", "hangzhou",
			"hefei", "hengshui", "heyuan", "heze", "huaian", "huhehaote",
			"huizhou", "huludao", "huzhou", "jiangmen", "jiaxing", "jinan",
			"jinhua", "jining", "kunming", "laiwu", "langfang", "lanzhou",
			"lasa", "lianyungang", "liaocheng", "linfen", "linyi", "lishui",
			"liuzhou", "nanchang", "nanjing", "nanning", "nantong", "ningbo",
			"panjin", "qingdao", "qingyuan", "qinhuangdao", "quanzhou",
			"quzhou", "rizhao", "shanghai", "shantou", "shaoguan", "shaoxing",
			"shenyang", "shenzhen", "shijiazhuang", "suqian", "suzhou",
			"taian", "taiyuan", "taizhou", "taizhoushi", "tangshan", "tianjin", "tongchuan",
			"weifang", "weihai", "weinan", "wenzhou", "wuhan", "wulumuqi",
			"wuxi", "xiamen", "xian", "xiangtan", "xianyang", "xingtai",
			"xining", "xuzhou", "yanan", "yancheng", "yangquan", "yangzhou",
			"yantai", "yinchuan", "yingkou", "yuxi", "zaozhuang",
			"zhangjiakou", "zhaoqing", "zhengzhou", "zhenjiang", "zhongshan",
			"zhoushan", "zhuhai", "zhuzhou", "zibo" };

	public static final HashMap<String, String> CITYMAP;
	static {
		CITYMAP = new HashMap<String, String>();
		CITYMAP.put("保定", "baoding");
		CITYMAP.put("宝鸡", "baoji");
		CITYMAP.put("包头", "baotou");
		CITYMAP.put("北海", "beihai");
		CITYMAP.put("北京", "beijing");
		CITYMAP.put("滨州", "binzhou");
		CITYMAP.put("沧州", "cangzhou");
		CITYMAP.put("长春", "changchun");
		CITYMAP.put("长沙", "changsha");
		CITYMAP.put("长治", "changzhi");
		CITYMAP.put("常州", "changzhou");
		CITYMAP.put("承德", "chengde");
		CITYMAP.put("成都", "chengdu");
		CITYMAP.put("重庆", "chongqing");
		CITYMAP.put("大连", "dalian");
		CITYMAP.put("丹东", "dandong");
		CITYMAP.put("大通", "datong");
		CITYMAP.put("德州", "dezhou");
		CITYMAP.put("东莞", "dongguan");
		CITYMAP.put("东营", "dongying");
		CITYMAP.put("鄂尔多斯", "eerduosi");
		CITYMAP.put("佛山", "foshan");
		CITYMAP.put("福州", "fuzhou");
		CITYMAP.put("广州", "guangzhou");
		CITYMAP.put("贵阳", "guiyang");
		CITYMAP.put("哈尔滨", "haerbin");
		CITYMAP.put("海口", "haikou");
		CITYMAP.put("邯郸", "handan");
		CITYMAP.put("杭州", "hangzhou");
		CITYMAP.put("合肥", "hefei");
		CITYMAP.put("衡水", "hengshui");
		CITYMAP.put("河源", "heyuan");
		CITYMAP.put("菏泽", "heze");
		CITYMAP.put("淮安", "huaian");
		CITYMAP.put("呼和浩特", "huhehaote");
		CITYMAP.put("惠州", "huizhou");
		CITYMAP.put("葫芦岛", "huludao");
		CITYMAP.put("湖州", "huzhou");
		CITYMAP.put("江门", "jiangmen");
		CITYMAP.put("嘉兴", "jiaxing");
		CITYMAP.put("济南", "jinan");
		CITYMAP.put("金华", "jinhua");
		CITYMAP.put("昆明", "kunming");
		CITYMAP.put("莱芜", "laiwu");
		CITYMAP.put("廊坊", "langfang");
		CITYMAP.put("兰州", "lanzhou");
		CITYMAP.put("拉萨", "lasa");
		CITYMAP.put("连云港", "lianyungang");
		CITYMAP.put("聊城", "liaocheng");
		CITYMAP.put("临汾", "linfen");
		CITYMAP.put("临沂", "linyi");
		CITYMAP.put("丽水", "lishui");
		CITYMAP.put("柳州", "liuzhou");
		CITYMAP.put("南昌", "nanchang");
		CITYMAP.put("南京", "nanjing");
		CITYMAP.put("南宁", "nanning");
		CITYMAP.put("南通", "nantong");
		CITYMAP.put("宁波", "ningbo");
		CITYMAP.put("盘锦", "panjin");
		CITYMAP.put("青岛", "qingdao");
		CITYMAP.put("清远", "qingyuan");
		CITYMAP.put("秦皇岛", "qinhuangdao");
		CITYMAP.put("泉州", "quanzhou");
		CITYMAP.put("衢州", "quzhou");
		CITYMAP.put("日照", "rizhao");
		//CITYMAP.put("沙门", "shamen");
		CITYMAP.put("上海", "shanghai");
		CITYMAP.put("汕头", "shantou");
		CITYMAP.put("韶关", "shaoguan");
		CITYMAP.put("绍兴", "shaoxing");
		CITYMAP.put("沈阳", "shenyang");
		CITYMAP.put("深圳", "shenzhen");
		CITYMAP.put("石家庄", "shijiazhuang");
		CITYMAP.put("宿迁", "suqian");
		CITYMAP.put("苏州", "suzhou");
		CITYMAP.put("太原", "taiyuan");
		CITYMAP.put("台州", "taizhou");
		
		CITYMAP.put("泰州", "taizhoushi");
		
		CITYMAP.put("唐山", "tangshan");
		CITYMAP.put("天津", "tianjin");
		CITYMAP.put("铜川", "tongchuan");
		CITYMAP.put("潍坊", "weifang");
		CITYMAP.put("威海", "weihai");
		CITYMAP.put("渭南", "weinan");
		
		CITYMAP.put("温州", "wenzhou");
		CITYMAP.put("武汉", "wuhan");
		CITYMAP.put("乌鲁木齐", "wulumuqi");
		CITYMAP.put("无锡", "wuxi");
		CITYMAP.put("厦门", "xiamen");
		CITYMAP.put("西安", "xian");
		CITYMAP.put("湘潭", "xiangtan");
		CITYMAP.put("咸阳", "xianyang");
		CITYMAP.put("邢台", "xingtai");
		CITYMAP.put("西宁", "xining");
		CITYMAP.put("徐州", "xuzhou");
		CITYMAP.put("延安", "yanan");
		CITYMAP.put("盐城", "yancheng");
		CITYMAP.put("阳泉", "yangquan");
		CITYMAP.put("扬州", "yangzhou");
		CITYMAP.put("烟台", "yantai");
		CITYMAP.put("银川", "yinchuan");
		CITYMAP.put("营口", "yingkou");
		CITYMAP.put("玉溪", "yuxi");
		CITYMAP.put("枣庄", "zaozhuang");
		CITYMAP.put("张家口", "zhangjiakou");
		CITYMAP.put("肇庆", "zhaoqing");
		CITYMAP.put("郑州", "zhengzhou");
		CITYMAP.put("镇江", "zhenjiang");
		CITYMAP.put("中山", "zhongshan");
		CITYMAP.put("舟山", "zhoushan");
		CITYMAP.put("珠海", "zhuhai");
		CITYMAP.put("株洲", "zhuzhou");
		CITYMAP.put("淄博", "zibo");
	}
}
